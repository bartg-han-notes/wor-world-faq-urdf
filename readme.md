# Launching Rviz with an URDF-file

1) First, open a terminal and go to the root folder of this repo (same folder where _config.bash_ is in).
2) Secondly, run "_config.bash_" with: _. config.bash_
3) Lastly, run "_run.bash_" with: _. run.bash_